package com.htc.springDemo.service;

import java.util.List;
import java.util.Locale.Category;

import com.htc.springDemo.dao.CategoryDAO;
import com.htc.springDemo.dao.ProductDAO;
import com.htc.springDemo.model.Product;

public class ProductService {
	
	ProductDAO productDAO;
	CategoryDAO categoryDAO;
	
	public ProductDAO getProductDAO() {
		return productDAO;
	}
	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}
	
	public CategoryDAO getCategoryDAO() {
		return categoryDAO;
	}
	public void setCategoryDAO(CategoryDAO categoryDAO) {
		this.categoryDAO = categoryDAO;
	}
	
	public boolean addProduct(Product p) {
		return productDAO.addProduct(p);
	}
	public Product getProduct(String productCode) {
		return null;
	}
	public List<Product> getProducts(String category){
		return null ;
	}
	public List<Product> getProducts(){
		return null;
	}
	public boolean removeProduct(String productCode) {
		return false;
	}
	public boolean updateProduct(String productCode, double newPrice, int qoh) {
		return false;
	}
	public boolean addCategory(Category category) {
		return false;
	}
	public Category getCategory(String category) {
		return null;
	}
	
	public boolean removeCategory(String category) {
		boolean productRemoveStatus = productDAO.removeProducts(category);
		boolean categoryRemoveStatus = categoryDAO.removeCategory(category);
		if(productRemoveStatus && categoryRemoveStatus)
		return true;
		else return false;
	}
	
	
}

