package com.htc.springDemo.main;


//package com.htc.spring5demo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.htc.springDemo.config.AppConfig;
import com.htc.springDemo.di.Customer;

public class DITest {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Customer cust = (Customer) context.getBean("customer");
		System.out.println(cust);
		cust.getAddress().setDoorno("55/55");
		cust.getAddress().setStreet("Street-2");
		System.out.println(cust.getAddress());
	
		System.out.println("------------------------------");
		
		Customer cust1 = (Customer) context.getBean("customer1");
		System.out.println(cust1);
		System.out.println(cust1.getAddress());
		
	}
}
