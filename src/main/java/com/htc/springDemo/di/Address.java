package com.htc.springDemo.di;

//package com.htc.spring5demo.di;

public class Address {

	private String addressId;
	private String doorno;
	private String street;
	private String city;
	
	public Address() {}

	public Address(String addressId, String doorno, String street, String city) {
		super();
		this.addressId = addressId;
		this.doorno = doorno;
		this.street = street;
		this.city = city;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getDoorno() {
		return doorno;
	}

	public void setDoorno(String doorno) {
		this.doorno = doorno;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", doorno=" + doorno + ", street=" + street + ", city=" + city + "]";
	}
	
}
