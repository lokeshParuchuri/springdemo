package com.htc.springDemo.dao;

import com.htc.springDemo.model.Category;

public interface CategoryDAO {
	public boolean addCategory(Category category);
	public Category getCategory(String category);
	public boolean removeCategory(String category);

}
