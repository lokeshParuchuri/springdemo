package com.htc.springDemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.htc.springDemo.di.Address;
import com.htc.springDemo.di.Customer;

@Configuration
public class AppConfig {		//ApplicationContext  container  configuration.

	@Bean(name="address")
	@Scope("prototype")
	public Address getAddress() {
		Address address = new Address();
		address.setAddressId("add01");
		address.setDoorno("56/22");
		address.setStreet("Street-1");
		address.setCity("Chennai");
		return address;
	}
	
	@Bean(name="customer")
	public Customer getCustomer() {
		Customer cust =new Customer();
		cust.setCustomerCode("c0001");
		cust.setCustomerName("ABC Electronics");
		cust.setAddress(getAddress());
		cust.setContactNo("959595955");
		return cust;
	}
	@Bean(name="customer1")
	public Customer getCustomer1() {
		Customer cust =new Customer("c002", "Ramesh Enterpise", getAddress(), "999595959");
		return cust;
	}
}
