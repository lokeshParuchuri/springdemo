package com.htc.springDemo.aspect;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAspect {

	Log logger;
	@Before(value="execution(* com.htc.spring5demo.dao.ProductDAOImpl.*(..))")
	public void beforeMethodCall(JoinPoint jp) {
		logger = LogFactory.getLog(jp.getTarget().getClass());
		logger.info("Calling method- " + jp.getSignature().getName());
		logger.info("Params:" + Arrays.toString(jp.getArgs()));
	}
	@AfterReturning(value="execution(* com.htc.spring5demo.dao.ProductDAOImpl.*(..))", returning="result")
	public void afterReturingFromMethod(JoinPoint jp, Object result) {
		logger = LogFactory.getLog(jp.getTarget().getClass());
		logger.info("Returning method- " + jp.getSignature().getName());
		logger.info("Return value:" + result);
	}
	@AfterThrowing(value="execution(* com.htc.spring5demo.dao.ProductDAOImpl.*(..))", throwing="ex")
	public void afterExceptionThrowing(JoinPoint jp, Throwable ex) {
		logger = LogFactory.getLog(jp.getTarget().getClass());
		logger.info("Returning method- " + jp.getSignature().getName());
		logger.error("Exception Occured:" + ex.toString());
	}
}
